import express from 'express';
import cors from 'cors';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import dotenv from 'dotenv';
import path from 'path';
import crypto, { createSecretKey } from 'crypto';
import nodemailer from 'nodemailer';
import { fileURLToPath } from 'url';
import { dirname } from 'path';
import jwt from 'jsonwebtoken';

dotenv.config();

const app = express();
const PORT = process.env.PORT || 5000;
const __dirname = dirname(fileURLToPath(import.meta.url));

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

mongoose
      .connect(process.env.MONGO_DB_CONNECTION_STRING, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
      })
      .then(() => {
            console.log('MongoDB Connected');
      })
      .catch((err) => {
            console.log('Error: ' + err);
      });

app.listen(PORT, () => {
      console.log(`Server started on port: ${PORT}`);
});

// ================== Secret Keys ==================
//

const secretKey = createSecretKey(Buffer.from(process.env.SECRET_KEY, 'hex'));

// ================== API Routes ==================
//

import User from './models/User.js';
import Post from './models/Post.js';

app.get('/hello', async (req, res) => {
      const data = req.query.data; // Accessing the query parameter
      res.send('<pre>Hello from backend. Data: ' + data + '</pre>');

      return res.status(200).json({ success: true });
});

app.post('/register', async (req, res) => {
      const { name, email, password, profileImage } = req.body;

      console.log('checking incomming values');

      if (!name || !email || !password) {
            return res.status(400).json({ message: 'Please enter all fields' });
      }

      console.log('checking if user exists');

      const existingUser = await User.findOne({ email });
      if (existingUser) {
            return res.status(400).json({ message: 'User with this email already exists' });
      }

      console.log('prepare user to add to database');

      try {
            // creeate new user
            const newUser = new User({
                  name,
                  email,
                  password,
                  profileImage,
            });

            // generate the verification token
            newUser.verificationToken = crypto.randomBytes(32).toString('hex');

            // save user
            await newUser.save();

            // send verification email
            sendVerificationEmail(newUser.email, newUser.verificationToken);

            res.status(200).json({ message: 'Registration successful. Please check your email for verification' });
      } catch (error) {
            res.status(400).json({ message: error.message });
      }
});

const sendVerificationEmail = async (email, verificationToken) => {
      console.log('sending verification email...');
      const user = process.env.MAILER_EMAIL;
      const pass = process.env.MAILER_PASSWORD;
      const transporter = nodemailer.createTransport({
            service: process.env.MAILER_SERVICE,
            auth: {
                  user,
                  pass,
            },
      });

      const mailOption = {
            from: process.env.MAILER_EMAIL,
            to: email,
            subject: 'Verify your email',
            text: `Please click the following link to verify your email: ${process.env.CLIENT_URL}/verify?token=${verificationToken}`,
      };

      try {
            await transporter.sendMail(mailOption);
            console.log('Verification email sent');
      } catch (error) {
            console.log('Error sending verification email: ', error);
      }
};

// api to verify email
app.get('/verify', async (req, res) => {
      const { token } = req.query;

      try {
            const user = await User.findOne({ verificationToken: token });
            if (!user) {
                  return res.status(404).json({ message: 'Invalid verification token' });
            }

            user.verified = true;
            user.verificationToken = null;
            await user.save();

            res.status(200).json({ message: 'Email verified successfully' });
      } catch (error) {
            res.status(400).json({ message: error.message });
      }
});

// api to login
app.post('/login', async (req, res) => {
      const { email, password } = req.body;
      console.log('found login');

      try {
            const user = await User.findOne({ email });
            if (!user) {
                  return res.status(404).json({ message: 'Login failed. Invalid email or password' });
            }

            if (user.password !== password) {
                  return res.status(404).json({ message: 'Login failed. Invalid password' });
            }

            const token = jwt.sign({ userId: user._id }, secretKey);
            res.status(200).json({ token });
      } catch (error) {
            console.log(error);
            res.status(500).json({ message: 'login failed!' });
      }
});

// user's profile
app.get('/get-user-profile/:userId', async (req, res) => {
      try {
            const userId = req.params.userId;
            const user = await User.findById(userId);
            if (!user) {
                  return res.status(404).json({ message: 'User not found' });
            }
            res.status(200).json({ user });
      } catch (error) {
            res.status(500).json({ message: error.message });
      }
});

// get all NOT connected users
app.get('/get-not-connected-users/:userId', async (req, res) => {
      try {
            // for testing return all users
            //return res.status(200).json({ users: await User.find() });

            // exclude the current user
            const userId = req.params.userId;
            const connectedUsers = await User.findById(userId).populate('connections', '_id');

            // if(!connectedUsers) {
            //       return res.status(400).json({ message: 'No users connected yet!' });
            // }

            const connectedUsersIds = connectedUsers.connections.map((user) => user._id);

            // find all users which are not connected
            const notConnectedUsers = await User.find({ _id: { $ne: userId, $nin: connectedUsersIds } });

            res.status(200).json({ users: notConnectedUsers });
      } catch (error) {
            res.status(500).json({ message: error.message });
      }
});

// send connection request
app.post('/connection-request', async (req, res) => {
      try {
            const { senderId, receiverId } = req.body;
            const sender = await User.findById(senderId);
            const receiver = await User.findById(receiverId);
            if (!sender || !receiver) {
                  return res.status(404).json({ message: 'Sender or receiver not found' });
            }

            // Add senderId to receiver's connectionRequests array if it doesn't already exist (addToSet is used to avoid duplicates)
            await User.findByIdAndUpdate(receiverId, { $addToSet: { connectionRequests: senderId } });

            // Add receiverId to sender's sentConnectionRequests array if it doesn't already exist (addToSet is used to avoid duplicates)
            await User.findByIdAndUpdate(senderId, { $addToSet: { sentConnectionRequests: receiverId } });

            res.status(200).json({ message: 'Connection request sent successfully' });
      } catch (error) {
            res.status(500).json({ message: error.message });
      }
});

// endpoint to show all connection requests
app.get('/get-connection-requests/:userId', async (req, res) => {
      try {
            const { userId } = req.params; //
            const user = await User.findById(userId).populate('connectionRequests', '_id name profileImage').lean();
            if (!user) {
                  return res.status(404).json({ message: 'User not found' });
            }
            const connectionRequests = user.connectionRequests;
            res.status(200).json({ connectionRequests });
      } catch (error) {
            res.status(500).json({ message: error.message });
      }
});

// endpoint to accept connection request
app.post('/connection-request/accept', async (req, res) => {
      try {
            const { senderId, receiverId } = req.body;
            const sender = await User.findById(senderId);
            const receiver = await User.findById(receiverId);
            if (!sender || !receiver) {
                  return res.status(404).json({ message: 'Sender or receiver not found' });
            }

            // // Add receiverId to sender's connections array
            // await User.findByIdAndUpdate(senderId, { $addToSet: { connections: receiverId } });

            // // Add senderId to receiver's connections array
            // await User.findByIdAndUpdate(receiverId, { $addToSet: { connections: senderId } });

            // // Remove senderId from receiver's connectionRequests array
            // await User.findByIdAndUpdate(receiverId, { $pull: { connectionRequests: senderId } });

            // // Remove receiverId from sender's sentConnectionRequests array
            // await User.findByIdAndUpdate(senderId, { $pull: { sentConnectionRequests: receiverId } });

            // optimized code for above
            await User.updateMany(
                  { _id: { $in: [senderId, receiverId] } },
                  {
                        $addToSet: { connections: { $each: [senderId, receiverId] } },
                        $pull: { connectionRequests: senderId, sentConnectionRequests: receiverId },
                  }
            );

            res.status(200).json({ message: 'Connection accepted successfully' });
      } catch (error) {
            res.status(500).json({ message: error.message });
      }
});
