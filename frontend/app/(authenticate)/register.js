import React from 'react';
import axios from 'axios';
import {
      StyleSheet,
      Text,
      View,
      SafeAreaView,
      Image,
      KeyboardAvoidingView,
      TextInput,
      Pressable,
      Alert,
      ScrollView,
} from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons';
import { Entypo } from '@expo/vector-icons';
import { useRouter } from 'expo-router';
import LogoLarge from '../../components/LogoLarge';
import styles from '../../styles/theme';
import { HOST_BACKEND } from '../../utils/constants';



const register = () => {
      const [email, setEmail] = React.useState('');
      const [password, setPassword] = React.useState('');
      const [name, setName] = React.useState('');
      const [image, setImage] = React.useState('');

      const router = useRouter();

      const onPressRegister_ = () => {
            console.log('onPressRegister');
            axios.post(HOST_BACKEND + 'hello')
                  .then((res) => {
                        console.log(res.data);
                  })
                  .catch((err) => {
                        console.log(err);
                  });
      };

      const onPressRegister = () => {
            console.log('onPressRegister');

            const user = {
                  name,
                  email,
                  password,
                  profileImage: image,
            };

            // validate all fields, show error alert if any field is not passing the validation

            if (!name || !email || !password) {
                  Alert.alert('Registration failed', 'Please enter all fields');
                  return;
            }

            if (password.length < 3) {
                  Alert.alert('Registration failed', 'Password must be at least 3 characters');
                  return;
            }

            if (!email.includes('@') || !email.includes('.')) {
                  Alert.alert('Registration failed', 'Please enter a valid email');
                  return;
            }

            axios.post(HOST_BACKEND + '/register', user)
                  .then((res) => {
                        console.log(res.data.message);
                        Alert.alert('Success', res.data.message);
                        setName('');
                        setEmail('');
                        setPassword('');
                        setImage('');
                  })
                  .catch((err) => {
                        if (err.response) {
                              // The request was made and the server responded with a status code
                              // that falls out of the range of 2xx
                              console.log(err.response.data);
                              console.log(err.response.status);
                              console.log(err.response.headers);
                        } else if (err.request) {
                              // The request was made but no response was received
                              // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                              // http.ClientRequest in node.js
                              console.log(err.request);
                        } else {
                              // Something happened in setting up the request that triggered an Error
                              console.log('Error', err.message);
                        }

                        console.log('Error: ' + err);
                        console.log('Error err.response:', err.response);
                        console.log('Error err.response.data:', err.response.data);
                        const message = err.response.data.message || 'Something went wrong while registering';
                        //Alert.alert('Registration failed', message);
                        Alert.alert('Registration failed', message);
                  });
      };

      return (
            <SafeAreaView style={styles.page}>
                  <LogoLarge />
                  <View style={styles.captionContainer}>
                        <Text style={styles.caption}>Register your new account</Text>
                        {/* <Text style={styles.captionSub}>{HOST_BACKEND}</Text> */}
                  </View>
                  <KeyboardAvoidingView behavior='height'>
                        <ScrollView style={{ marginEnd: 20, marginStart: 20 }}>
                              <View style={[styles.mt_l, styles.loginFormContainer]}>
                                    <View style={styles.loginInputContainer}>
                                          <Ionicons name='person' size={24} color='grey' style={styles.loginInputContainerIcon} />
                                          <TextInput
                                                style={styles.loginInput}
                                                placeholder='enter your name'
                                                value={name}
                                                onChangeText={(text) => setName(text)}
                                          />
                                    </View>
                                    <View style={styles.loginInputContainer}>
                                          <MaterialIcons
                                                name='email'
                                                size={24}
                                                color='grey'
                                                style={styles.loginInputContainerIcon}
                                          />
                                          <TextInput
                                                style={styles.loginInput}
                                                placeholder='enter your email'
                                                value={email}
                                                onChangeText={(text) => setEmail(text)}
                                          />
                                    </View>
                                    <View style={styles.loginInputContainer}>
                                          <AntDesign name='lock' size={24} color='grey' style={styles.loginInputContainerIcon} />
                                          <TextInput
                                                style={styles.loginInput}
                                                placeholder='enter your password'
                                                secureTextEntry
                                                value={password}
                                                onChangeText={(text) => setPassword(text)}
                                          />
                                    </View>
                                    <View style={styles.loginInputContainer}>
                                          <Entypo name='image' size={24} color='grey' style={styles.loginInputContainerIcon} />
                                          <TextInput
                                                style={styles.loginInput}
                                                placeholder='enter image url'
                                                value={image}
                                                onChangeText={(text) => setImage(text)}
                                          />
                                    </View>
                              </View>

                              <View style={[styles.mt_l, styles.buttonContainer]}>
                                    <Pressable style={styles.blueButton} onPress={onPressRegister}>
                                          <Text style={styles.blueButtonText}>Register</Text>
                                    </Pressable>
                                    <Pressable onPress={() => router.replace('login')}>
                                          <Text style={[styles.infoText, styles.mt_m]}>Back to login screen</Text>
                                    </Pressable>
                              </View>
                        </ScrollView>
                  </KeyboardAvoidingView>
            </SafeAreaView>
      );
};

export default register;
