import { StyleSheet, Text, View, SafeAreaView, Image, KeyboardAvoidingView, TextInput, Pressable } from 'react-native';
import React, { useEffect } from 'react';
import { MaterialIcons } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';
import { useRouter } from 'expo-router';
import LogoLarge from '../../components/LogoLarge';
import styles from '../../styles/theme';
import axios from 'axios';
import { Alert } from 'react-native';
import { HOST_BACKEND } from '../../utils/constants';
import AsyncStorage from '@react-native-async-storage/async-storage';

const login = () => {
      const [email, setEmail] = React.useState('');
      const [password, setPassword] = React.useState('');
      const router = useRouter();

      useEffect(() => {
            const checkLoginStatus = async () => {
                  try {
                        const token = await AsyncStorage.getItem('authToken');
                        if (token) {
                              console.log('token exist in async storage, replacing route, token: ', token);
                              router.replace('/(tabs)/home');
                        }
                  } catch (error) {
                        console.log(error);
                  }
            };

            const clearAsyncStorage = async () => {
                  // do something
            };

            clearAsyncStorage();

            checkLoginStatus();
      }, []);

      const onPressLogin = async () => {
            const user = {
                  email,
                  password,
            };

            try {
                  if (!email || !password) {
                        throw new Error('Please enter all fields');
                  }

                  const response = await axios.post(HOST_BACKEND + '/login', user);
                  console.log(response.data);
                  AsyncStorage.setItem('authToken', response.data.token);
                  console.log('Login successful, going to home, token: ', response.data.token);
                  router.replace('/(tabs)/home');
            } catch (error) {
              console.log("some error", error);
                  //  error handling
                  if (error.response && error.response.data && error.response.data.message) {
                        Alert.alert('Login failed', error.response.data.message); // Display specific backend error message
                  } else {
                        console.error('Login failed:', error); // Log full error for debugging
                        Alert.alert('Login failed', 'Something went wrong, please try again'); // Generic error message
                  }
            }
      };

      return (
            <SafeAreaView style={styles.page}>
                  <LogoLarge />

                  <KeyboardAvoidingView>
                        <View style={styles.captionContainer}>
                              <Text style={styles.caption}>Log in to your account</Text>
                        </View>
                        <View style={[styles.mt_l, styles.loginFormContainer]}>
                              <View style={styles.loginInputContainer}>
                                    <MaterialIcons name='email' size={24} color='grey' style={styles.loginInputContainerIcon} />
                                    <TextInput
                                          style={styles.loginInput}
                                          placeholder='enter your email'
                                          value={email}
                                          onChangeText={(text) => setEmail(text)}
                                    />
                              </View>
                              <View style={styles.loginInputContainer}>
                                    <AntDesign name='lock' size={24} color='grey' style={styles.loginInputContainerIcon} />
                                    <TextInput
                                          style={styles.loginInput}
                                          placeholder='enter your password'
                                          secureTextEntry
                                          value={password}
                                          onChangeText={(text) => setPassword(text)}
                                    />
                              </View>
                        </View>

                        <View style={styles.linksContainer}>
                              <Text style={styles.linksText}>Keep me logged in</Text>
                              <Text style={styles.linksText}>Forgot password?</Text>
                        </View>

                        <View style={[styles.mt_l, styles.buttonContainer]}>
                              <Pressable style={styles.blueButton} onPress={onPressLogin}>
                                    <Text style={styles.blueButtonText}>Log in</Text>
                              </Pressable>
                              <Pressable onPress={() => router.replace('register')}>
                                    <Text style={[styles.infoText, styles.mt_m]}>Don't have an account? Sign up</Text>
                              </Pressable>
                        </View>
                  </KeyboardAvoidingView>
            </SafeAreaView>
      );
};

export default login;
