import { Pressable, StyleSheet, Text, View } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useRouter } from 'expo-router';
import styles from '../../../styles/theme';

const index = () => {
      const router = useRouter();
      const logout = async () => {
            try {
              // TODO we need also create and call the logout endpoint later
                  await AsyncStorage.removeItem('authToken');
                  console.log('Logged out successfully');

                  console.log('going to login');
                  router.replace('/(authenticate)/login');
            } catch (e) {
                  console.log(e);
            }
      };

      return (
            <View>
                  <Text>index of home</Text>

                  <View style={[styles.mt_l, styles.buttonContainer]}>
                        <Pressable style={styles.blueButton} onPress={logout}>
                              <Text style={styles.blueButtonText}>Log out</Text>
                        </Pressable>
                  </View>
            </View>
      );
};
// alexandermastny1@gmx.at
export default index;
