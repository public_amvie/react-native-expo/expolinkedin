import { StyleSheet, Text, View, ScrollView, Pressable, FlatList } from 'react-native';
import React, { useState, useEffect } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { HOST_BACKEND } from '../../../utils/constants';
import JWT from 'expo-jwt';
import RowLink from '../../../components/RowLink';
import { Entypo } from '@expo/vector-icons';
import UserProfileCard from '../../../components/UserProfileCard';
import axios from 'axios';

const index = () => {
      const [userId, setUserId] = useState();
      const [error, setError] = useState();
      const [user, setUser] = useState();
      const [users, setUsers] = useState();
      const [connectionRequests, setConnectionRequests] = useState();

      useEffect(() => {
            const fetchUser = async () => {
                  try {
                        const token = await AsyncStorage.getItem('authToken');

                        if (token) {
                              console.log('now decode the token: ', token);
                              const decoded = JWT.decode(token, '');
                              setUserId(decoded.userId);
                        }
                  } catch (error) {
                        setError('Error fetching user ID: ' + error.message);
                  }
            };

            fetchUser();
      }, []);

      useEffect(() => {
            if (userId) {
                  fetchUserProfile();
                  fetchUsers();
                  fetchConnectionRequests();
            }
      }, [userId]);

      const fetchUserProfile = async () => {
            console.log('fetchUserProfile for userId', userId);
            try {
                  const { data } = await axios.get(`${HOST_BACKEND}/get-user-profile/${userId}`);
                  setUser(data.user);
                  //console.log(data.user);
            } catch (error) {
                  setError('Error fetching user profile: ' + error.message);
            }
      };

      const fetchUsers = async () => {
            try {
                  const { data } = await axios.get(`${HOST_BACKEND}/get-not-connected-users/${userId}`);
                  setUsers(data.users);
            } catch (error) {
                  setError('Error fetching users: ' + error.message);
            }
      };

      const fetchConnectionRequests = async () => {
            try {
                  const { data } = await axios.get(`${HOST_BACKEND}/get-connection-requests/${userId}`);
                  const connectionRequests = data.connectionRequests?.map((request) => ({
                        _id: request._id,
                        name: request.name,
                        email: request.email,
                        profileImage: request.profileImage,
                  }));

                  setConnectionRequests(connectionRequests);
            } catch (error) {
                  setError('Error fetching users: ' + error.message);
            }
      };

      useEffect(() => {
            if (error) {
                  console.log(error);
            }
      }, [error]);

      return (
            <View style={{ flex: 1 }}>
                  <RowLink text='Network' />
                  <RowLink text='Invitations (0)' />
                  <View>{/* show all of the request connections */}</View>
                  <View style={{ marginHorizontal: 15 }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                              <Text>Grow your network faster</Text>
                              <Entypo name='cross' size={24} color='black' />
                        </View>
                        <Text>Find and contact the right people. Plus see who has viewed your profile.</Text>
                        <View
                              style={{
                                    backgroundColor: '#ffc72c',
                                    width: 140,
                                    paddingHorizontal: 10,
                                    paddingVertical: 5,
                                    borderRadius: 25,
                                    marginTop: 8,
                              }}
                        >
                              <Text style={{ textAlign: 'center', color: 'white', fontWeight: 'bold' }}>Try Premium</Text>
                        </View>
                  </View>

                  <View>
                        <Text style={{ marginHorizontal: 15, marginTop: 15, fontWeight: 'bold' }}>Get new connections</Text>
                  </View>
                  <FlatList
                        data={users}
                        columnWrapperStyle={{ justifyContent: 'space-between' }}
                        numColumns={2}
                        keyExtractor={(item) => item._id}
                        renderItem={({ item }) => <UserProfileCard item={item} userId={userId} />}
                  />

                  <View>
                        <Text style={{ marginHorizontal: 15, marginTop: 15, fontWeight: 'bold' }}>Connection Requests</Text>
                  </View>
                  <FlatList
                        data={connectionRequests}
                        columnWrapperStyle={{ justifyContent: 'space-between' }}
                        numColumns={2}
                        keyExtractor={(item) => item._id}
                        renderItem={({ item }) => <UserProfileCard item={item} userId={userId} />}
                  />
                  
            </View>
      );
};

export default index;

const styles = StyleSheet.create({});
