import React, { useState, useEffect } from 'react';
import { View, Text, FlatList, SafeAreaView, ScrollView, Pressable, SectionList } from 'react-native';
import RowLink from '../../../components/RowLink';

export default function App() {
      const [data, setData] = useState([]);

      const [followers, setFollowers] = useState([]);
      const [friends, setFriends] = useState([]);

      useEffect(() => {
            const followers = [];
            for (let i = 0; i < 20; i++) {
                  followers.push({ id: i, name: `follower Item ${i}`, type: 'follower' });
            }
            setFollowers(followers);
            const friends = [];
            for (let i = 30; i < 50; i++) {
                  friends.push({ id: i , name: `follower Item ${i}`, type: 'friend' });
            }
            setFriends(friends);
      }, []);

      const dataT = [
            { title: 'Followers', data: followers, type: 'follower' },
            { title: 'Friends', data: friends, type: 'friend' },
            {
                  id : 60,
                  title: 'Following',
                  data: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
                  type: 'header',
            },
      ];

      return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>

                  <View style={{ flex: 1 }}>
                        <SectionList
                              sections={dataT}
                              keyExtractor={(item, index) => item.type}
                              renderSectionHeader={({ section: { title } }) => (
                                    <Text
                                          style={{
                                                fontSize: 20,
                                                backgroundColor: '#f0f0f0',
                                                padding: 20,
                                          }}
                                    >
                                          {title}
                                    </Text>
                              )}
                              renderItem={({ item }) => {
                                    if (item.type === 'header' || item.type === 'follower') {
                                          return (
                                                <View>
                                                      <Text style={{ padding: 20, fontSize: 18 }}>{item.data}</Text>
                                                </View>
                                          );
                                    } else {
                                          return <RowLink text={item.name} onPress={() => console.log('handle row click')} />;
                                    }
                              }}
                        />
                  </View>
            </SafeAreaView>
      );
}
