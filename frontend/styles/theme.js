import { StyleSheet } from 'react-native';

export default Theme = StyleSheet.create({
    mt_m: {
          marginTop: 30,
    },
    mt_l: {
          marginTop: 70,
    },
    captionContainer: {
          alignItems: 'center',
    },
    caption: {
          fontSize: 17,
          fontWeight: 'bold',
          marginTop: 12,
          marginBottom: 12,
          color: '#041E42',
    },
    page: {
          flex: 1,
          alignItems: 'center',
          backgroundColor: 'silver',
    },
    loginFormContainer: {
          flexDirection: 'column',
          alignItems: 'center',
          gap: 15,
    },
    loginInputContainer: {
          flexDirection: 'row',
          alignItems: 'center',
          gap: 5,
          backgroundColor: '#e0e0e0',
          borderRadius: 5,
          padding: 8,
    },
    loginInputContainerIcon: {
          paddingLeft: 15,
    },
    loginInput: {
          color: 'gray',
          marginVertical: 10,
          width: 300,
          fontSize: 19,
          marginLeft: 8
    },
    linksContainer: {
          marginTop: 30,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
    },
    linksText: {
          color: '#007fff',
          fontWeight: 'bold',
    },
    infoText: {
          color: '#666',
          fontWeight: 'bold',
    },
    buttonContainer: {
          flexDirection: 'column',
          alignItems: 'center',
    },
    blueButton: {
          backgroundColor: '#0072b1',
          width: 200,
          padding: 20,
          borderRadius: 5,
          alignItems: 'center',
          justifyContent: 'center',
    },
    blueButtonText: {
          color: 'white',
          fontWeight: 'bold',
          fontSize: 16,
    },
});
