import { StyleSheet, Text, View, Dimensions, Image, Pressable } from 'react-native';
import { HOST_BACKEND } from '../utils/constants';
import { useState } from 'react';

const UserProfileCard = ({ item, userId }) => {

      const [connectionSent, setConnectionSent] = useState(false);

      console.log("item", item);

      const onSendConnectionRequest = async () => {
            console.log("userId", userId); // loggeded in user
            console.log("item._id", item._id); // other user which is displayed in card

            try {
                  const response = await fetch(`${HOST_BACKEND}/connection-request`, {
                        method: 'POST',
                        headers: {
                              'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({ senderId: userId, receiverId: item._id }),
                  });
                  const data = await response.json();
                  console.log('data', data);

                  if(response.ok) {
                        console.log('Connection request sent successfully');
                        setConnectionSent(true);
                  }

            } catch (error) {
                  console.log(error);
            }

            // const response = await fetch(`${HOST_BACKEND}/send-connection-request/${userId}/${item.id}`);
            // const data = await response.json();
            // console.log('data', data);
      }
      return (
            <View
                  style={{
                        flex: 1,
                        borderRadius: 9,
                        marginHorizontal: 16,
                        borderColor: '#e0e0e0',
                        borderWidth: 1,
                        marginVertical: 10,
                        alignItems: 'center',
                        height: Dimensions.get('window').height / 4,
                        width: Dimensions.get('window').width - 80 / 2,
                        justifyContent: 'center',
                  }}
            >
                  <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Image
                              source={{ uri: item?.profileImage }}
                              style={{ width: 90, height: 90, borderRadius: 45, resizeMode: 'cover', backgroundColor: 'blue' }}
                        />
                  </View>

                  <View style={{ marginTop: 10 }}>
                        <Text style={{ textAlign: 'center', fontSize: 16, fontWeight: 'bold' }}>{item?.name}</Text>
                        <Text style={{ textAlign: 'center', fontSize: 14, marginHorizontal: 10, marginTop: 5 }}>
                              Engineer Graduate | LinkedIn member
                        </Text>
                  </View>

                  <Pressable
                        onPress={onSendConnectionRequest}
                        style={{
                              marginLeft: 'auto',
                              marginRight: 'auto',
                              borderColor: connectionSent || item?.connectionRequests?.includes(userId) ? 'gray' : '#0072b1',
                              borderWidth: 1,
                              borderRadius: 25,
                              marginTop: 7,
                              paddingHorizontal: 15,
                              paddingVertical: 4,
                        }}
                  >
                        <Text
                              style={{
                                    fontSize: 16,
                                    fontWeight: 'bold',
                                    color: connectionSent || item?.connectionRequests?.includes(userId) ? 'gray' : '#0072b1',
                              }}
                        >
                              {connectionSent || item?.connectionRequests?.includes(userId) ? 'Pending' : 'Connect'}
                        </Text>
                  </Pressable>
            </View>
      );
};

export default UserProfileCard;

const styles = StyleSheet.create({});
