import { View } from 'react-native';

const Divider = () => {
      return <View style={{ borderColor: '#e0e0e0', borderWidth: 2, marginVertical: 10 }} />;
};

export default Divider;
