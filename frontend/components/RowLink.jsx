import { Pressable, StyleSheet, Text, View } from 'react-native';
import React from 'react';
import { AntDesign } from '@expo/vector-icons';
import Divider from './Divider';

const styles = StyleSheet.create({
      rowLink: {
            marginTop: 10,
            marginHorizontal: 10,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
      },
      rowLinkText: {
            fontSize: 16,
            fontWeight: 'bold',
      },
});

const RowLink = (props) => {
      return (
            <>
                  <Pressable style={styles.rowLink}>
                        <Text style={styles.rowLinkText}>{props.text}</Text>
                        <AntDesign name='arrowright' size={24} color='black' />
                  </Pressable>
                  <Divider />
            </>
      );
};

export default RowLink;
