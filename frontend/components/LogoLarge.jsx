import { StyleSheet, View, Image, Text } from 'react-native';
import React from 'react';

const LogoLarge = () => {
      return (
            <>
                  <View style={{ flexDirection: 'row', alignItems: 'center', gap: 10 }}>
                        <Image
                              source={require('../assets/expo-logo.png')}
                              style={{ width: 40, height: 40}}
                              resizeMode='contain'
                        />
                        <Image
                              source={require('../assets/linkedin-logo-png-1840.png')}
                              style={{ width: 200, height: 150 }}
                              resizeMode='contain'
                        />
                  </View>
            </>
      );
};

export default LogoLarge;

const styles = StyleSheet.create({});
